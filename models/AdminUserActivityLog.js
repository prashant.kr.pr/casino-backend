const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AdminUserActivityLogSchema = new Schema({
  name: { type: String, required: true },
  admin_code: { type: String, required: true },
  admin_name: { type: String, required: true },
  old_value: { type: String, required: true },
  new_value: { type: String, required: true },
  remark: { type: String, required: true },
  email: { type: String, required: true },
  ip: { type: String, required: true },
  report_type: { type: String, required: true },
  admin_type: { type: String, required: true },
  created_at: { type: Date, default: Date.now }
});

AdminUserActivityLogSchema.pre('save', function () {
  now = new Date();
  if (!this.created_at) {
    this.created_at = now;
  }
  next();
})

module.exports = AdminUserActivityLog = mongoose.model("adminUserActivityLog", AdminUserActivityLogSchema);
