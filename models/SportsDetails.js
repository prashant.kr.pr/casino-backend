const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SportsDetailsSchema = new Schema({
  name: { type: String, required: true },
  sport_details: {
    type: Array,
    properties: {
      0: {
        team_one: { type: String, required:true },
        team_two: { type: String },
        contact_person: { type: String, required: true },
        country: { type: String }
      }
    }
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

SportsDetailsSchema.pre('save', function () {
  now = new Date();
  this.updated_at = now;
  if (!this.created_at) {
    this.created_at = now;
  }
  next();
})

module.exports = SportsDetails = mongoose.model("sportsDetails", SportsDetailsSchema);
