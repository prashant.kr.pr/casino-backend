const mongoose = require("mongoose");
const mongoosePaginate  = require('mongoose-paginate');
const { USER_ROLES } = require("../utils/constants");
const Schema = mongoose.Schema;

const STATUS = [true, false];
const COMMISSION_TYPE = ['Bet by Bet', 'Only on Minus', 'No Comm'];
const PARTNERSHIP_TYPE = ['Fix', 'Changeable'];

const AdminMasterUserDetailSchema = new Schema({
  code: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  reference: { type: String },
  role: { type: String, required: true, enum: USER_ROLES },
  roleOrder: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  contactNo: { type: Number, required: true, unique: true },
  city: { type: String, default: null },
  doj: { type: Date, default: Date.now },
  coins: { type: Number, default: 0 },
  used_coins: { type: Number, default: 0 },
  limit: { type: Number, default: 0 },
  locked: { type: Boolean, default: false },
  dead: { type: String, default: false },
  superadmin_code: { type: String, required: true },
  superadmin_name: { type: String, required: true },
  admin_code: { type: String, default: null },
  admin_name: { type: String, default: null },
  subadmin_code: { type: String, default: null },
  subadmin_name: { type: String, default: null },
  supermaster_code: { type: String, default: null },
  supermaster_name: { type: String, default: null },
  masteragent_code: { type: String, default: null },
  masteragent_name: { type: String, default: null },
  superagent_code: { type: String, default: null },
  superagent_name: { type: String, default: null },
  agent_code: { type: String, default: null },
  agent_name: { type: String, default: null },
  client_code: { type: String, default: null },
  client_name: { type: String, default: null },
  sport_type: {
    type: Array,
    properties: {
      0: {
        sport_type: { type: String },
        commission_type: { type: String, default: null, enum: COMMISSION_TYPE },
        match_commission: { type: Number, default: 0 },
        session_commission: { type: Number, default: 0 },
        bet_delay: { type: Number, default: 0 },
        session_delay: { type: Number, default: 0 },
        min_match_stake: { type: Number, default: 0 },
        max_match_stake: { type: Number, default: 0 },
        min_session_stake: { type: Number, default: 0 },
        max_session_stake: { type: Number, default: 0 },
        max_profit: { type: Number, default: 0 },
        max_loss: { type: Number, default: 0 },
        session_max_profit: { type: Number, default: 0 },
        session_max_loss: { type: Number, default: 0 },
        min_exposure: { type: Number, default: 0 },
        max_exposure: { type: Number, default: 0 },
        winning_limit: { type: Number, default: 0 },
        mob_app_charges: { type: Number, default: 0 },
        partnership_percent: { type: Number, default: 0 },
        partnership_type: { type: String, default: null, enum: PARTNERSHIP_TYPE },
        sba: { type: Number, default: 0 }
      }
    }
  },
  user_status: { type: String, default: false, enum: STATUS },
  logging_status: { type: String, default: false, enum: STATUS },
  betting_status: { type: String, default: false, enum: STATUS },
  added_by: { type: Date, default: Date.now },
  modified_by: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

AdminMasterUserDetailSchema.pre('save', function () {
  now = new Date();
  this.updated_at = now;
  if (!this.created_at) {
    this.created_at = now;
  }
  next();
})
AdminMasterUserDetailSchema.plugin(mongoosePaginate);
module.exports = AdminMasterUserDetail = mongoose.model("adminMasterUserDetail", AdminMasterUserDetailSchema);
