const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MatchSessionSchema = new Schema({
  matchId: { type: String, required: true },
  sessionName:{type: String, required: true },
  matchType:{type: String},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

MatchSessionSchema.pre('save', function () {
  now = new Date();
  this.updated_at = now;
  if (!this.created_at) {
    this.created_at = now;
  }
  next();
})

module.exports = MatchSession = mongoose.model("matchsessions", MatchSessionSchema);
