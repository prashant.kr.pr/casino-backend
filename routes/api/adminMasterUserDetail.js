const express = require("express");
const router = express.Router();
// const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const keys = require("../../config/keys");
const AdminMasterUserDetail = require("../../models/AdminMasterUserDetail");
const {
    HTTP_SERVER_ERROR,
    DATA_CREATED_FAILED,
    DATA_CREATED_SUCCESSFULLY,
    HTTP_SUCCESS,
    HTTP_SERVER_ERROR_TEXT,
    MISSING_PARAMS,
    DATA_UPDATED_SUCCESSFULLY,
    DATA_UPDATION_FAILED,
    USER_ROLES_TYPE_DETAILS
} = require("../../utils/constants");
const validateAdminMasterUserDetails = require("../../validation/validateAdminMasterUserDetails");
const validateLoginInput = require("../../validation/login");
const { verifyToken } = require("./verifyToken");


/**
 * API to create admin user
 */
router.post('/create-adminuser', verifyToken, async (request, res) => {
    try {
        const responseData = request.body;
        const { isValid, error } = validateAdminMasterUserDetails(responseData);

        if (!isValid) {
            return res.status(HTTP_SUCCESS).send({
                success: isValid,
                error
            });
        }
        let code = '';
        AdminMasterUserDetail.
            countDocuments({ role: responseData.role })
            .exec((err, count) => {
                if (err) {
                    res.status(HTTP_SERVER_ERROR).send({
                        success: false,
                        message: `${DATA_CREATED_FAILED} ${err}`
                    });
                }
                for (const [key, value] of Object.entries(USER_ROLES_TYPE_DETAILS)) {
                    if (key == responseData.role) {
                        var code_prefix = value.code_prefix;
                        code = code_prefix + (count + 1);
                        let data = { ...responseData, code };

                        AdminMasterUserDetail.create(data, (err) => {
                            if (err) {
                                res.status(HTTP_SERVER_ERROR).send({
                                    success: false,
                                    message: `${DATA_CREATED_FAILED} ${err}`
                                });
                            } else {
                                res.status(HTTP_SUCCESS).send({
                                    success: true,
                                    message: DATA_CREATED_SUCCESSFULLY
                                });
                            }
                        })
                    }

                }
            });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e
        });
    }
});
/**
 * API to get all the admin users
 */
router.get('/get-all-adminuser', verifyToken, async (request, res) => {
    try {
        const data = await AdminMasterUserDetail.find();
        return res.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});

/**
 * API to get single admin user
 * @param userId String Id of user which is saved in the DB.
 */
router.get('/get-single-adminuser/:id', verifyToken, async (request, res) => {
    try {
        const userId = request.params.id;
        if (!userId) {
            return {
                success: false,
                message: MISSING_PARAMS,
                data: {}
            }
        }

        const data = await AdminMasterUserDetail.
            findById(userId);
        return res.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});


/**
 * API to update the data of user/admin
 * @param userId String Id of user which is saved in the DB.
 * 
 * Issue:- If primary keys are already saved and unchanged values are comming then it will throw error.
 */
router.post('/update-admin-user/:userId', verifyToken, async (request, res) => {
    try {
        const userId = request.params.userId;
        const body = request.body;
        AdminMasterUserDetail.findByIdAndUpdate(
            userId,
            { $set: body },
            (err) => {
                if (err) {
                    res.status(HTTP_SERVER_ERROR).send({
                        success: false,
                        message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                    });
                } else {
                    const adminMasterData = AdminMasterUserDetail.findById(userId);
                    adminMasterData.exec(function (err, doc) {
                        if (err) {
                            res.status(HTTP_SERVER_ERROR).send({
                                success: false,
                                message: `${HTTP_SERVER_ERROR_TEXT} ${err.errmsg}`
                            });
                        } else {
                            res.status(HTTP_SUCCESS).send({
                                success: true,
                                data: doc,
                                message: DATA_UPDATED_SUCCESSFULLY
                            });
                        }
                    })

                }
            }
        )
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});

/**
 * This API will update the coins of user.
 */
router.post('/update-child-admin-coins/:userId', verifyToken, async (request, res) => {
    try {
        const userId = request.params.userId;
        const body = request.body.updatedData;
        AdminMasterUserDetail.findByIdAndUpdate(
            userId,
            { $set: body },
            (err) => {
                if (err) {
                    res.status(HTTP_SERVER_ERROR).send({
                        success: false,
                        message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                    });
                } else {
                    const adminMasterData = AdminMasterUserDetail.findById(userId);
                    adminMasterData.exec(function (err, doc) {
                        if (err) {
                            res.status(HTTP_SERVER_ERROR).send({
                                success: false,
                                message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                            });
                        } else {

                            const parentAdminCode = request.body.parentAdmincode;
                            const role = request.body.role;
                            const roleOrder = parseInt(request.body.roleOrder);

                            let totalCoinsQuery = '';
                            if (role === "admin") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), admin_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            } else if (role === "subAdmin") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), subadmin_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            } else if (role === "superMaster") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), supermaster_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            } else if (role === "masterAgent") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), masteragent_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            } else if (role === "superAgent") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), superagent_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            } else if (role === "agent") {
                                totalCoinsQuery = AdminMasterUserDetail
                                    .aggregate([{ $match: { roleOrder: (roleOrder + 1), agent_code: parentAdminCode } },
                                    { $group: { _id: "", totalCoins: { $sum: "$coins" } } }]);
                            }
                            let used_coins = 0;
                            totalCoinsQuery.exec((err, doc) => {
                                if (err) {
                                    res.status(HTTP_SUCCESS).send({
                                        success: false,
                                        error: `Unable to update data.`
                                    });
                                } else {
                                    if (doc.length) {
                                        used_coins = doc ? doc && doc[0] && doc[0].totalCoins : 0;
                                    } else {
                                        used_coins = 0;
                                    }
                                    AdminMasterUserDetail.updateOne({ "code": request.body.parentAdmincode }, { used_coins }, (err) => {
                                        if (err) {
                                            res.status(HTTP_SUCCESS).send({
                                                success: false,
                                                message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                                            });
                                        } else {
                                            res.status(HTTP_SUCCESS).send({
                                                success: true,
                                                message: DATA_UPDATED_SUCCESSFULLY
                                            });
                                        }
                                    })
                                }
                            });
                        }
                    })
                }
            }
        )
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});


/**
 * In progress
 */
router.post('/listOfAdminUsers/:role', verifyToken, (request, res) => {
    try {
        const { page, limit } = request.query;
        const role = request.params.role;
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(limit) || 10,
        };
        const loggedInUserRole = request.body.loggedInUserRole;
        const loggedInUserCode = request.body.loggedInUserCode;

        if (loggedInUserRole === "subAdmin") {
            AdminMasterUserDetail.paginate({ role, subadmin_code: loggedInUserCode }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        } else if (loggedInUserRole === "superMaster") {
            AdminMasterUserDetail.paginate({ role, supermaster_code: loggedInUserCode }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        } else if (loggedInUserRole === "masterAgent") {
            AdminMasterUserDetail.paginate({ role, masteragent_code: loggedInUserCode }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        } else if (loggedInUserRole === "superAgent") {
            AdminMasterUserDetail.paginate({ role, superagent_code: loggedInUserCode }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        } else if (loggedInUserRole === "agent") {
            AdminMasterUserDetail.paginate({ role, agent_code: loggedInUserCode }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        } else {
            AdminMasterUserDetail.paginate({ role }, options).then(function (result) {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: result
                });
            });
        }

    } catch (error) {
        res.status(400).send({
            success: false,
            error: `Unable to load data`
        });
    }
});
/**
 * @param roleOrder number Role Order of User
 * @param isSameAsLoggedIn if the value is true then we change the query and return the details of only one user by _id
 * @param _id string
 */
router.post('/listOfAdminUsersRole', verifyToken, (request, res) => {
    try {
        const roleOrder = request.body.roleOrder;
        const isSameAsLoggedIn = request.body.isSameAsLoggedIn;
        const _id = request.body._id;
        let userDetails = '';
        if (isSameAsLoggedIn) {
            userDetails = AdminMasterUserDetail.find({ roleOrder, _id }).select('admin_code admin_name agent_code agent_name client_code client_name masteragent_code masteragent_name subadmin_code subadmin_name superadmin_code superadmin_name superagent_code superagent_name supermaster_code supermaster_name name code sport_type');
        } else {
            userDetails = AdminMasterUserDetail.find({ roleOrder }).select('admin_code admin_name agent_code agent_name client_code client_name masteragent_code masteragent_name subadmin_code subadmin_name superadmin_code superadmin_name superagent_code superagent_name supermaster_code supermaster_name name code sport_type');
        }

        userDetails.exec((err, doc) => {
            if (err) {
                res.status(HTTP_SUCCESS).send({
                    success: false,
                    error: `Unable to load data`
                });
            } else {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: doc
                })
            }
        })
    } catch (e) {
        res.status(400).send({
            success: false,
            error: `Unable to load data`
        });
    }
});

/**
 * Update the bulk status of the customer
*/
router.post('/update-admin-user-status', verifyToken, async (request, res) => {
    try {
        const selectedIds = request.body.selectedIds;
        const status = request.body.status;

        AdminMasterUserDetail.updateMany({ "_id": { $in: selectedIds } }, { user_status: status }, (err) => {
            if (err) {
                res.status(HTTP_SUCCESS).send({
                    success: false,
                    message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                });
            } else {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    message: DATA_UPDATED_SUCCESSFULLY
                });
            }
        })
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});

/**
 * Update the bulk coins of the customer
*/
router.post('/update-admin-user-coins', verifyToken, async (request, res) => {
    try {
        const selectedIds = request.body.selectedIds;
        const coins = request.body.coins;

        AdminMasterUserDetail.updateMany({ "_id": { $in: selectedIds } }, { coins }, (err) => {
            if (err) {
                res.status(HTTP_SUCCESS).send({
                    success: false,
                    message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                });
            } else {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    message: DATA_UPDATED_SUCCESSFULLY
                });
            }
        })
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});

router.post("/login", (request, res) => {
    try {
        const { error, isValid } = validateLoginInput(request.body);
        if (!isValid) {
            return res.status(200).json({ success: false, error });
        }
        const code = request.body.code;
        const password = request.body.password;
        AdminMasterUserDetail.findOne({ code }).then((user) => {
            if (!user) {
                return res.status(200).json({ success: false, error: "Code not found." });
            }
            if (password === user.password) {
                const payload = {
                    id: user._id,
                    name: user.name,
                    role: user.role,
                    code: user.code,
                    roleOrder: user.roleOrder,
                    email: user.email,
                    superadmin_code: user.superadmin_code,
                    superadmin_name: user.superadmin_name
                };
                jwt.sign(
                    payload,
                    keys.secretOrKey,
                    {
                        expiresIn: 31556926, // 1 year in seconds
                    },
                    (err, token) => {
                        if (err)
                            res.status(500).json({ error: "Error signing token", raw: err });
                        res.json({
                            success: true,
                            token
                        });
                    })
            } else {
                return res
                    .status(200)
                    .json({ success: false, error: "Password Incorrect." });
            }
        });
    }
    catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});
/**
 * Function to get the child admin users
 */
router.post('/getChildAdminUsers', (request, res) => {
    try {
        const parentAdminCode = request.body.parentAdmincode;
        const role = request.body.role;
        const roleOrder = request.body.roleOrder;

        let userDetails = '';
        if (role === "admin") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), admin_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        } else if (role === "subAdmin") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), subadmin_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        } else if (role === "superMaster") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), supermaster_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        } else if (role === "masterAgent") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), masteragent_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        } else if (role === "superAgent") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), superagent_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        } else if (role === "agent") {
            userDetails = AdminMasterUserDetail
                .find({ roleOrder: (roleOrder + 1), agent_code: parentAdminCode })
                .select('code role roleOrder name coins used_coins');
        }

        userDetails.exec((err, doc) => {
            if (err) {
                res.status(HTTP_SUCCESS).send({
                    success: false,
                    error: `Unable to load data`
                });
            } else {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    data: doc
                })
            }
        })
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});

/**
 * API to get single user coins
 * @param userCode String User code which is saved in the DB.
 */
router.get('/get-single-user-coins/:code', verifyToken, async (request, res) => {
    try {
        const userCode = request.params.code;
        if (!userCode) {
            return {
                success: false,
                message: MISSING_PARAMS,
                data: {}
            }
        }
        const data = await AdminMasterUserDetail.
            find({ code: userCode }).select('coins');
        return res.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});

router.post('/toggle-admin-user/:userId/:currentTabRoleOrder/:adminCode', verifyToken, async (request, res) => {
    try {
        const userId = request.params.userId;
        const currentTabRoleOrder = request.params.currentTabRoleOrder;
        const adminCode = request.params.adminCode;
        const body = request.body;
        let countData = {};
        let data = "";

        if (currentTabRoleOrder == 2) {
            data = await AdminMasterUserDetail.find({ "admin_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.subAdmin = 0;
            countData.superMaster = 0;
            countData.masterAgent = 0;
            countData.masterAgent = 0;
            countData.superAgent = 0;
            countData.agent = 0;
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "subAdmin") {
                    countData.subAdmin++;
                } else if (ele.role === "superMaster") {
                    countData.superMaster++;
                } else if (ele.role === "masterAgent") {
                    countData.masterAgent++;
                } else if (ele.role === "superAgent") {
                    countData.superAgent++;
                } else if (ele.role === "agent") {
                    countData.agent++;
                } else if (ele.role === "client") {
                    countData.client++;
                }
            });
        } else if (currentTabRoleOrder == 3) {
            data = await AdminMasterUserDetail.find({ "subadmin_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.superMaster = 0;
            countData.masterAgent = 0;
            countData.masterAgent = 0;
            countData.superAgent = 0;
            countData.agent = 0;
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "superMaster") {
                    countData.superMaster++;
                } else if (ele.role === "masterAgent") {
                    countData.masterAgent++;
                } else if (ele.role === "superAgent") {
                    countData.superAgent++;
                } else if (ele.role === "agent") {
                    countData.agent++;
                } else if (ele.role === "client") {
                    countData.client++;
                }
            });
        } else if (currentTabRoleOrder == 4) {
            data = await AdminMasterUserDetail.find({ "supermaster_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.masterAgent = 0;
            countData.masterAgent = 0;
            countData.superAgent = 0;
            countData.agent = 0;
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "masterAgent") {
                    countData.masterAgent++;
                } else if (ele.role === "superAgent") {
                    countData.superAgent++;
                } else if (ele.role === "agent") {
                    countData.agent++;
                } else if (ele.role === "client") {
                    countData.client++;
                }
            });
        } else if (currentTabRoleOrder == 5) {
            data = await AdminMasterUserDetail.find({ "masteragent_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.superAgent = 0;
            countData.agent = 0;
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "superAgent") {
                    countData.superAgent++;
                } else if (ele.role === "agent") {
                    countData.agent++;
                } else if (ele.role === "client") {
                    countData.client++;
                }
            });
        } else if (currentTabRoleOrder == 6) {
            data = await AdminMasterUserDetail.find({ "superagent_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.agent = 0;
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "superAgent") {
                    countData.superAgent++;
                } else if (ele.role === "agent") {
                    countData.agent++;
                } else if (ele.role === "client") {
                    countData.client++;
                }
            });
        } else if (currentTabRoleOrder == 7) {
            data = await AdminMasterUserDetail.find({ "agent_code": adminCode, roleOrder: { $gt: currentTabRoleOrder } }).select('role');
            countData.client = 0;

            data.map(ele => {
                if (ele.role === "client") {
                    countData.client++;
                }
            });
        }
        AdminMasterUserDetail.findByIdAndUpdate(
            userId,
            { $set: body },
            (err) => {
                if (err) {
                    res.status(HTTP_SERVER_ERROR).send({
                        success: false,
                        message: `${DATA_UPDATION_FAILED} ${err.errmsg}`
                    });
                } else {
                    const adminMasterData = AdminMasterUserDetail.findById(userId);
                    adminMasterData.exec(function (err, doc) {
                        if (err) {
                            res.status(HTTP_SERVER_ERROR).send({
                                success: false,
                                message: `${HTTP_SERVER_ERROR_TEXT} ${err.errmsg}`
                            });
                        } else {
                            countData && res.status(HTTP_SUCCESS).send({
                                success: true,
                                data: countData,
                                message: DATA_UPDATED_SUCCESSFULLY
                            });
                        }
                    })

                }
            }
        )
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e.errmsg
        });
    }
});

module.exports = router;