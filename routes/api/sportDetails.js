const express = require("express");
const router = express.Router();

const SportsDetails = require("../../models/SportsDetails");
const {
    HTTP_SERVER_ERROR,
    DATA_CREATED_FAILED,
    DATA_CREATED_SUCCESSFULLY,
    HTTP_SUCCESS,
    HTTP_SERVER_ERROR_TEXT,
} = require("../../utils/constants");


/**
 * API to create sports data
 */
router.post('/create-sport-details', async (request, reply) => {
    try {
        const data = request.body;
        SportsDetails.create(data, (err) => {
            if (err) {
                reply.status(HTTP_SERVER_ERROR).send({
                    success: false,
                    message: `${DATA_CREATED_FAILED} ${err}`
                });
            } else {
                reply.status(HTTP_SUCCESS).send({
                    success: true,
                    message: DATA_CREATED_SUCCESSFULLY
                });
            }
        })
    } catch (e) {
        reply.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e
        });
    }
});

// /**
//  * API to get all the admin users
//  */
router.get('/get-all-sports-details', async (request, reply) => {
    try {
        const data = await SportsDetails.find();
        return reply.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        reply.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});

module.exports = router;