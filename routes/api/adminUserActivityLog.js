const express = require("express");
const moment = require("moment");
const router = express.Router();

const AdminUserActivityLog = require("../../models/AdminUserActivityLog");
const {
    HTTP_SERVER_ERROR,
    DATA_CREATED_FAILED,
    DATA_CREATED_SUCCESSFULLY,
    HTTP_SUCCESS,
    HTTP_SERVER_ERROR_TEXT
} = require("../../utils/constants");
const { verifyToken } = require("./verifyToken");


/**
 * API to create admin user activity log
 */
router.post('/create-user-activity-log',verifyToken, async (request, res) => {
    try {
        const responseData = request.body;
        AdminUserActivityLog.create(responseData, (err) => {
            if (err) {
                res.status(HTTP_SERVER_ERROR).send({
                    success: false,
                    message: `${DATA_CREATED_FAILED} ${err}`
                });
            } else {
                res.status(HTTP_SUCCESS).send({
                    success: true,
                    message: DATA_CREATED_SUCCESSFULLY
                });
            }
        })
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e
        });
    }
});
/**
 * API to get all the admin users activity log
 */
router.post('/get-all-admin-user-activity-log',verifyToken, async (request, res) => {
    try {
        let formData = request.body;
        let toDate = new Date(formData.toDate)
        const data = await AdminUserActivityLog.find({
            report_type: formData.reportType,
            $and: [
                {created_at: {$gte: formData.fromDate}},
                {created_at: {$lte: moment(new Date(toDate)).add(1, 'days').format('YYYY-MM-D')}}
            ],
            name: formData.selectedAdminUser
        }).sort({"created_at":-1});
        return res.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});

module.exports = router;