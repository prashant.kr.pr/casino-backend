const jwt = require("jsonwebtoken");
const { secretOrKey } = require("../../config/keys");
const { INVALID_TOKEN } = require("../../utils/constants");


module.exports.verifyToken = function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        req.token = bearerHeader;

        //verifying user
        jwt.verify(req.token, secretOrKey, function (err, decoded) {
            if (err) {
                return res.json({
                    status: 401,
                    success: false,
                    error: INVALID_TOKEN
                });
            } else {
                next();
            }
        });
    } else {
        return res.json({
            status: 401,
            success: false,
            error: INVALID_TOKEN
        });
    }
}