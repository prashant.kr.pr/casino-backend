const express = require("express");
const router = express.Router();
// const bcrypt = require("bcryptjs");

const MatchSession = require("../../models/MatchSession");
const {
    HTTP_SERVER_ERROR,
    DATA_CREATED_FAILED,
    DATA_CREATED_SUCCESSFULLY,
    HTTP_SUCCESS,
    HTTP_SERVER_ERROR_TEXT,
    MISSING_PARAMS,
    DATA_UPDATED_SUCCESSFULLY,
    DATA_UPDATION_FAILED,
    USER_ROLES_TYPE_DETAILS
} = require("../../utils/constants");
const { verifyToken } = require("./verifyToken");


/**
 * API to create sports data
 */
router.post('/create-match-sessions', async (request, reply) => {
    try {
        const data = request.body;
        MatchSession.create(data, (err) => {
            if (err) {
                reply.status(HTTP_SERVER_ERROR).send({
                    success: false,
                    message: `${DATA_CREATED_FAILED} ${err}`
                });
            } else {
                reply.status(HTTP_SUCCESS).send({
                    success: true,
                    message: DATA_CREATED_SUCCESSFULLY
                });
            }
        })
    } catch (e) {
        reply.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: e
        });
    }
});

router.get('/get-all-match-sessions', verifyToken, async (request, res) => {
    try {
        const data = await MatchSession.find();
        return res.status(HTTP_SUCCESS).send({
            success: true,
            data: data
        });
    } catch (e) {
        res.status(HTTP_SERVER_ERROR).send({
            success: false,
            message: HTTP_SERVER_ERROR_TEXT
        });
    }
});


module.exports = router;