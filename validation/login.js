const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateLoginInput(data) {
  let error = "";

  // Convert empty fields to an empty string so we can use validator functions
  data.code = !isEmpty(data.code) ? data.code : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  // code checks
  if (Validator.isEmpty(data.code)) {
    error = "Code field is required.";
  } 
  // else if (!Validator.isEmail(data.email)) {
  //   error = "Email is invalid";
  // }
  // Password checks
  if (Validator.isEmpty(data.password)) {
    error = "Password field is required.";
  }
  return {
    error,
    isValid: isEmpty(error),
  };
};
