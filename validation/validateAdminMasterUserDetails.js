const Validator = require("validator");
// const isEmpty = require("is-empty");
var isAlphanumeric = require('is-alphanumeric');
// var isNumeric = require("isnumeric");


function isEmail(val) {
  let regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!regEmail.test(val)) {
    return true;
  } else {
    return false;
  }
};


function isIntegerNumber(value) {
  if(isNaN){
    var num = /^-?[0-9]+$/;
    result = num.test(value);
    if (result) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

//check empty value, undefined value and positive value
function isEmpty(val){
  return (val === undefined || val == null || val <= 0) ? true : false;
}

function checkFloatNumber(value) {
  var num = /^[-+]?[0-9]+\.[0-9]+$/;
  result = num.test(value);
  if (result) {
    return true;
  } else {
    return false;
  }
}

function isNumber(value){
  return typeof value === 'number' && isFinite(value);
}

/**
 * Still a number of validations are pending. Check if mob number or email are unique
 *  
 */
module.exports = function validateAdminMasterUserDetails(data) {
  let error = [];

  function sportFieldValidate(sport_type, sport_object) {
    let error_new = {};
    try {
      if (sport_object.hasOwnProperty("commission_type")) {
        if (Validator.isEmpty(sport_object.commission_type)) {
          error_new['commission_type'] = "Commission Type is required.";
        }
      } else {
        error_new['commission_type'] = "Commission Type is required.";
      }

      if (sport_object.hasOwnProperty("match_commission")) {
        if (!sport_object.match_commission) {
          error_new['match_commission'] = "Match Commission is required.";
        } else if (!isNumber(sport_object.match_commission)) {   //check match_commission: is-number or not : accepted int or float
          error_new['match_commission'] = "Please enter a valid number";
        }
      } else {
        error_new['match_commission'] = "Match Commission is required.";
      }

      if (sport_object.hasOwnProperty("session_commission")) {
        if (isEmpty(sport_object.session_commission)) {
          error_new['session_commission'] = "Session Commission is required.";
        } else if (!isNumber(sport_object.session_commission)) {   //check Session commission: is-number or not: : accepted int or float
          error_new['session_commission'] = "Please enter a valid number";
        }
      } else {
        error_new['session_commission'] = "Session Commission is required.";
      }

      if (sport_object.hasOwnProperty("bet_delay")) {
        if (isEmpty(sport_object.bet_delay)) {
          error_new['bet_delay'] = "Bet Delay is required.";
        } else if (!isIntegerNumber(sport_object.bet_delay)) {   //check Bet delay: is-numeric or not
          error_new['bet_delay'] = "Please enter a valid number.";
        }
      } else {
        error_new['bet_delay'] = "Bet Delay is required.";
      }

      if (sport_object.hasOwnProperty("session_delay")) {
        if (isEmpty(sport_object.session_delay)) {
          error_new['session_delay'] = "Session Delay is required.";
        } else if (!isIntegerNumber(sport_object.session_delay)) {   //check Session delay: is-numeric or not
          error_new['session_delay'] = "Please enter a valid number";
        }
      } else {
        error_new['session_delay'] = "Session Delay is required.";
      }

      if (sport_object.hasOwnProperty("min_match_stake")) {
        if (isEmpty(sport_object.min_match_stake)) {
          error_new['min_match_stake'] = "Min Match Stake is required.";
        } else if (!isIntegerNumber(sport_object.min_match_stake)) {   //check min_match_stake: is-numeric or not
          error_new['min_match_stake'] = "Please enter a valid number";
        }
      } else {
        error_new['min_match_stake'] = "Min Match Stake is required.";
      }

      if (sport_object.hasOwnProperty("max_match_stake")) {
        if (isEmpty(sport_object.max_match_stake)) {
          error_new['max_match_stake'] = "Max Match Stake is required.";
        } else if (!isIntegerNumber(sport_object.max_match_stake)) {   //check max_match_stake: is-numeric or not
          error_new['max_match_stake'] = "Please enter a valid number";
        }
      } else {
        error_new['max_match_stake'] = "Max Match Stake is required.";
      }

      if (sport_object.hasOwnProperty("min_session_stake")) {
        if (isEmpty(sport_object.min_session_stake)) {
          error_new['min_session_stake'] = "Min Session Stake is required.";
        } else if (!isIntegerNumber(sport_object.min_session_stake)) {   //check min_session_stake: is-numeric or not
          error_new['min_session_stake'] = "Please enter a valid number";
        }
      } else {
        error_new['min_session_stake'] = "Min Session Stake is required.";
      }

      if (sport_object.hasOwnProperty("max_session_stake")) {
        if (isEmpty(sport_object.max_session_stake)) {
          error_new['max_session_stake'] = "Max Session Stake is required.";
        } else if (!isIntegerNumber(sport_object.max_session_stake)) {   //check max_session_stake: is-numeric or not
          error_new['max_session_stake'] = "Please enter a valid number";
        }
      } else {
        error_new['max_session_stake'] = "Max Session Stake is required.";
      }

      if (sport_object.hasOwnProperty("max_profit")) {
        if (isEmpty(sport_object.max_profit)) {
          error_new['max_profit'] = "Max Profit is required.";
        } else if (!isIntegerNumber(sport_object.max_profit)) {   //check Max Profit: is-numeric or not
          error_new['max_profit'] = "Please enter a valid number";
        }
      } else {
        error_new['max_profit'] = "Max Profit is required.";
      }

      if (sport_object.hasOwnProperty("max_loss")) {
        if (isEmpty(sport_object.max_loss)) {
          error_new['max_loss'] = "Max Loss is required.";
        } else if (!isIntegerNumber(sport_object.max_loss)) {   //check Max loss: is-numeric or not
          error_new['max_loss'] = "Please enter a valid number";
        }
      } else {
        error_new['max_loss'] = "Max Loss is required.";
      }

      if (sport_object.hasOwnProperty("session_max_profit")) {
        if (isEmpty(sport_object.session_max_profit)) {
          error_new['session_max_profit'] = "Session Max Profit is required.";
        } else if (!isIntegerNumber(sport_object.session_max_profit)) {   //check session_max_profit: is-numeric or not
          error_new['session_max_profit'] = "Please enter a valid number";
        }
      } else {
        error_new['session_max_profit'] = "Session Max Profit is required.";
      }

      if (sport_object.hasOwnProperty("session_max_loss")) {
        if (isEmpty(sport_object.session_max_loss)) {
          error_new['session_max_loss'] = "Session Max Loss is required.";
        } else if (!isIntegerNumber(sport_object.session_max_loss)) {   //check session_max_loss: is-numeric or not
          error_new['session_max_loss'] = "Please enter a valid number";
        }
      } else {
        error_new['session_max_loss'] = "Session Max Loss is required.";
      }

      if (sport_object.hasOwnProperty("min_exposure")) {
        if (isEmpty(sport_object.min_exposure)) {
          error_new['min_exposure'] = "Min Exposure is required.";
        } else if (!isIntegerNumber(sport_object.min_exposure)) {   //check min_exposure: is-numeric or not
          error_new['min_exposure'] = "Please enter a valid number";
        }
      } else {
        error_new['min_exposure'] = "Min Exposure is required.";
      }

      if (sport_object.hasOwnProperty("max_exposure")) {
        if (isEmpty(sport_object.max_exposure)) {
          error_new['max_exposure'] = "Max Exposure is required.";
        } else if (!isIntegerNumber(sport_object.max_exposure)) {   //check max_exposure: is-numeric or not
          error_new['max_exposure'] = "Please enter a valid number";
        }
      } else {
        error_new['max_exposure'] = "Max Exposure is required.";
      }

      if (sport_object.hasOwnProperty("mob_app_charges")) {
        if (isEmpty(sport_object.mob_app_charges)) {
          error_new['mob_app_charges'] = "Mobile App charges is required.";
        } else if (!isIntegerNumber(sport_object.mob_app_charges)) {   //check mob_app_charges: is-numeric or not
          error_new['mob_app_charges'] = "Please enter a valid number";
        }
      } else {
        error_new['mob_app_charges'] = "Mobile App charges is required.";
      }

      if (sport_object.hasOwnProperty("partnership_percent")) {
        if (isEmpty(sport_object.partnership_percent)) {
          error_new['partnership_percent'] = "Partnership percent is required.";
        } else if (!isIntegerNumber(sport_object.partnership_percent)) {   //check partnership_percent: is-numeric or not
          error_new['partnership_percent'] = "Partnership percent should be numeric.";
        }
      } else {
        error_new['partnership_percent'] = "Partnership percent is required.";
      }

      if (sport_object.hasOwnProperty("partnership_type")) {
        if (isEmpty(sport_object.partnership_type)) {
          error_new['partnership_type'] = "Partnership Type is required.";
        }
      } else {
        error_new['partnership_type'] = "Partnership Type is required.";
      }

      if (sport_object.hasOwnProperty("sba")) {
        if (isEmpty(sport_object.sba)) {
          error_new['sba'] = "SBA is required.";
        } else if (!isIntegerNumber(sport_object.sba)) {   //check sba: is-numeric or not
          error_new['sba'] = "Please enter a valid number";
        }
      } else {
        error_new['sba'] = "SBA is required.";
      }
    } catch (e) {
      error_new['error'] = e.message;
    }

    return error_new;
  }


  try {
    //check empty Code
    if (data.hasOwnProperty("code")) {
      if (Validator.isEmpty(data.code)) {
        error['code'] = "Code is required.";
      }
    } else {
      error['code'] = "Code is required.";
    }


    //check empty name
    if (data.hasOwnProperty("name")) {
      if (Validator.isEmpty(data.name)) {
        error['name'] = "Name is required.";
      } else if (!Validator.isAlphanumeric(data.name)) {   //check name: isAlphanumeric or not
        error['name'] = "Name should be alphanumeric.";
      }
    } else {
      error['name'] = "Name is required.";
    }

    //check empty reference
    if (data.hasOwnProperty("reference")) {
      if (Validator.isEmpty(data.reference)) {
        error['reference'] = "Reference is required.";
      }
    } else {
      error['reference'] = "Reference is required.";
    }

    //check empty role
    if (data.hasOwnProperty("role")) {
      if (Validator.isEmpty(data.role)) {
        error['role'] = "Role is required.";
      }
    } else {
      error['role'] = "Role is required.";
    }

    //check empty role order
    if (data.hasOwnProperty("roleOrder")) {
      if (data.roleOrder === undefined || data.roleOrder === "") {
        error['roleOrder'] = "Role Order is required.";
      }
      else if (!isNumber(data.roleOrder)) {   //check role order: is-numeric or not
        error['roleOrder'] = "Role Order should be numeric.";
      }
    } else {
      error['roleOrder'] = "Role Order is required.";
    }

    //check empty email
    if (data.hasOwnProperty("email")) {
      if (Validator.isEmpty(data.email)) {
        error['email'] = "Email is required.";
      } else if (!Validator.isEmail(data.email)) {  //check email is correct or not
        error['email'] = "Email should be valid";
      }
    } else {
      error['email'] = "Email is required.";
    }

    //check empty contact number
    if (data.hasOwnProperty("contactNo")) {
      if (!data.contactNo) {
        error['contactNo'] = "Contact number is required.";
      } else if (data.contactNo.toString().length != 10) {  //check contact number length
        error['contactNo'] = "Contact number should be of 10 digits";
      } else if (!isNumber(data.contactNo)) {  //check contact number is numeric or not
        error['contactNo'] = "Contact number should be numeric";
      }
    } else {
      error['contactNo'] = "Contact number is required.";
    }

    //check empty city
    if (data.hasOwnProperty("city")) {
      if (Validator.isEmpty(data.city)) {
        error['city'] = "City is required.";
      } else if (!Validator.isAlphanumeric(data.city)) {   //check city: isAlphanumeric or not
        error['city'] = "City should be alphanumeric.";
      }
    } else {
      error['city'] = "City is required.";
    }

    // console.log(data.sport_type.length);
    for (let i = 0; i < data.sport_type.length; i++) {
      sport_type = data.sport_type[i]['sport'];
      sport_object = data.sport_type[i];
      error[sport_type] = sportFieldValidate(sport_type, sport_object);
    }
  } catch (e) {
    error['error'] = e.message;
  }

  return {
    error,
    isValid: isEmpty(error),
  };
};
