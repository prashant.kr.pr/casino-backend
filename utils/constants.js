/**
 * This file will contain all the constant of the system.
 */
// module.exports.PORT = 4000;

const userRoleTypeDetails = {
    "superAdmin" : {
        name: "Super Admin",
        order: 1,
        code_prefix: "SUA"
    },
    "admin" : {
        name: "Admin",
        order: 2,
        code_prefix: "ADMUSER"
    },
    "subAdmin" : {
       name: "Sub Admin",
       order: 3,
       code_prefix: "SB"
   },
   "superMaster" : {
       name: "Super Master",
       order: 4,
       code_prefix: "SM"
   },
   "masterAgent" : {
       name: "Master Agent",
       order: 5,
       code_prefix: "MA"
   },
   "superAgent" : {
       name: "Super Agent",
       order: 6,
       code_prefix: "SA"
   },
   "agent" : {
       name: "Agent",
       order: 7,
       code_prefix: "A"
   },
   "client" : {
       name: "Client",
       order: 8,
       code_prefix: "C"
   },
   "powerUser" : {
        name: "Power User",
        order: 9,
        code_prefix: "PU"
    },
}


module.exports.PAYMENT_TYPE = ["wallet", "card"];
module.exports.USER_ROLES = Object.keys(userRoleTypeDetails);
module.exports.USER_ROLES_TYPE_DETAILS = userRoleTypeDetails;

module.exports.MISSING_PARAMS = 'Required parameter missing.';
module.exports.HTTP_SERVER_ERROR_TEXT = 'Server Error.';
module.exports.DATA_UPDATED_SUCCESSFULLY = 'Data Updated Successfully';
module.exports.DATA_UPDATION_FAILED = 'Data Updation Failed.';
module.exports.DATA_DELETED_SUCCESSFULLY = 'Data Deleted Successfully';
module.exports.DATA_DELETION_FAILED = 'Data Deletion Failed.';
module.exports.DATA_CREATED_SUCCESSFULLY = 'Data Created Successfully.';
module.exports.DATA_CREATED_FAILED = 'Data Creation Failed.';
module.exports.DATA_FETCHED_SUCCESSFULLY = 'Data Fetched Successfully.';
module.exports.DATA_FETCHING_FAILED = 'Data Fetching Failed.';
module.exports.DATA_NOT_FOUND = 'Data Not Found.';

module.exports.INVALID_TOKEN = 'Invalid Token';

module.exports.HTTP_SUCCESS = 200;
module.exports.HTTP_BAD_REQUEST = 400;
module.exports.HTTP_FORBIDDEN = 403;
module.exports.HTTP_NOT_FOUND = 404;
module.exports.HTTP_SERVER_ERROR = 500;