const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const bodyParser = require("body-parser");
const passport = require("passport");
const indexRouter = require("./routes")
const users = require("./routes/api/users");
const adminMasterUserDetail = require("./routes/api/adminMasterUserDetail");
const adminUserActivityLog = require("./routes/api/adminUserActivityLog");
const sportDetails = require("./routes/api/sportDetails");
const matchSession = require("./routes/api/matchSession");

// DB Config
const db = require("./config/keys").mongoURI;

const app = express();

// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
); 
app.use(bodyParser.json());
app.disable( 'X-Powered-By' );

// Connect to MongoDB
mongoose
  .connect(db, { useUnifiedTopology: true ,useNewUrlParser: true })
  .then(() => console.log("MongoDB successfully connected."))
  .catch((err) => console.log(err));

app.use(cors());

// Passport middleware
app.use(passport.initialize());
// Passport configuration
require("./config/passport")(passport);
// Routes
app.use("/",indexRouter);
app.use("/api/users", users);
app.use("/api/adminMasterUserDetail", adminMasterUserDetail);
app.use("/api/sportDetails", sportDetails);
app.use("/api/adminUserActivityLog", adminUserActivityLog);
app.use("/api/matchSession", matchSession);

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Server up and running on port ${port} !`));
